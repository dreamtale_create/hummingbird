# 发票类
class Receipt:
    """
        receiptType 发票类型
        receiptCode 发票代码
        receiptNo   发票编号
        receiptAmount   发票金额
        receiptDate 开票日期
        startDate 上车时间
        endDate 下车时间
    """
    def __init__(self, receiptType, receiptCode, receiptNo, receiptAmount, receiptDate, startDate, endDate):
        self.receiptType = receiptType
        self.receiptCode = receiptCode
        self.receiptNo = receiptNo
        self.receiptAmount = receiptAmount
        self.receiptDate = receiptDate
        self.startDate = startDate
        self.endDate = endDate


