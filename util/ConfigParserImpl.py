import configparser


class ConfigParserImpl(configparser.ConfigParser):
    """
    set ConfigParser options for case sensitive.
    """
    def __init__(self, defaults=None):
        configparser.ConfigParser.__init__(self, defaults=defaults)

    def optionxform(self, value):
        return value
