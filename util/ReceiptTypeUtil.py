import json

receiptTypeMap = {}


def readConfig():
    """"
    读取发票类型配置文件
    """
    configDict = {}
    with open('config/receiptType.json', 'r', encoding='utf-8') as file:
        config = json.load(file)
        # config = {value: key for key, value in config.items()}
        for configItem in config:
            configDict[configItem['value']] = configItem['name']

    return configDict
    # return config
